#!/usr/bin/env node

const inquirer = require('inquirer')
const path = require('path')
const { execSync } = require('child_process')

const REPOSITORY_URL = `https://gitlab.com/mlaopane/express-typescript.git`
const currentDirPath = process.cwd()
const questions = {
    whatIsTheProjectName: {
        name: 'what-is-the-project-name',
        type: 'input',
        message: 'What is the project name ? ',
        validate: function(input) {
            // Project name may only include letters, numbers, underscores and hashes.
            if (/^([A-Za-z\-\_\d])+$/.test(input)) {
                return true
            }

            return false
        },
    }
}

function cloneRepository({ projectPath }) {
    try {
        execSync(`git clone ${REPOSITORY_URL} ${projectPath}`)
        execSync(`rm -rf ${path.join(projectPath, '.git')}`)
    } catch (error) {
        execSync(`rm -rf ${projectPath}`)
    }
}

function createProjectFromAnswers(answers) {
    const projectName = answers[questions.whatIsTheProjectName.name]
    const projectPath = path.join(currentDirPath, projectName)
    cloneRepository({ projectPath })
}

inquirer
    .prompt([questions.whatIsTheProjectName])
    .then(createProjectFromAnswers)
