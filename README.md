# make-express-ts-app

## Usage

Create your application with npx

```bash
npx make-express-ts-app
```

Or install the command globally

```bash
npm i -g make-express-ts-app
make-express-ts-app
```
